\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

%\usepackage[margin=2.54cm]{geometry}

%\usepackage{amsmath}
%\usepackage{amssymb}
\usepackage{siunitx}

\usepackage{tikz}
\usetikzlibrary{chains,scopes,arrows.meta}

\usepackage{import}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}
%\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[section]{placeins}

\usepackage[backend=biber]{biblatex}
\addbibresource{iot_reports.bib}

\newcommand{\routerpic}{\includegraphics[width=10mm]{router}}
\newcommand{\gwpic}{\includegraphics[width=10mm]{generic_gateway}}

\author{Riccardo Zanol}
%\date{April 18, 2017}
\title{%
  {Repairing mechanisms in RPL} \\%
  {\large Internet of Things and Smart Cities - Homework 1}%
}

\begin{document}
\maketitle

\graphicspath{{cisco-icons/bw/}}

\section{Introduction}
The Routing Protocol for Low-Power and Lossy Networks (RPL) is a
distance-vector based routing protocol designed by the ROLL group of
the IEFT. The ROLL group designed RPL to be a protocol suitable for
use in wireless networks of resource-constrained devices, which present
the need of frequent re-optimization of the routes as the conditions
change. This homework summarizes the re-optimization strategies defined
by the relevant RFC \cite{rfc6550} which are employed by LLN networks
that use RPL as a routing protocol.

Beside the RFC, the main sources of information on RPL are some
surveys, like chapter 17 of \cite{vasseur-iot}, or \cite{rpl-survey}
that describe the its functioning, but they are not always up to date.
Specifically \cite{vasseur-iot} refers to a draft version of the
standard, so some terminology has been changed, even if most of the
description of the protocol is still in agreement with the latest RFC
version.

RPL constructs destination-oriented acyclic graphs (DODAGs) of routes
toward and from a special node, the root, that typically acts as the
gateway to the Internet or to some other external network of the
LLN. Before discussing the mechanisms used by RPL to detect the need
for a repair of the routes and then to perform the actual repair, this
homework quickly reviews the regular route construction rules of RPL
and introduces a basic objective function from \cite{rfc6552} to be
used in the examples of the following sections.

\subsection{Route construction summary}
The construction of the DODAG can be quickly summarized as follows:
\begin{itemize}
\item upon initialization, the root node starts to multicast DIO
  messages (DODAG Information Object) to its neighbors. This messages
  contain the parameters for the DODAG and the metric objects related
  to the specific objective function in use;
\item the nodes that receive DIO messages from the root or from other
  neighbors can learn which of those neighbors are closer to the root,
  in the sense that their rank has a smaller value, and they also
  learn the path cost of a route through each of them (via the
  embedded metric objects);
\item they select some of their neighbors to be ``DODAG parents'',
  according to the specific OF rules, which take into account the metric
  values and ensure the satisfaction of the constraints, and one to be
  the ``preferred parent'' that will be the next hop in the path
  toward the root;
\item then they update the metric objects to reflect the additional
  cost of the hop to each possible parent, replace the advertised rank
  with their own rank, and forward DIO messages to their neighbors
  using multicast.
\end{itemize}

If the ``mode of operation'' also supports downward routes, the nodes
send unicast DAO messages (Destination Advertisement Object) upward,
either to their direct parents (if the DODAG is operating in ``storing
mode'') or to the root (if the DODAG is in ``non-storing'' mode). The
DAOs distribute knowledge of the addresses reachable through the nodes
of the RPL network. The subset of the DODAG parents of each node to
which it will send DAO messages is called ``DAO parent set'' and it,
too, is selected in an implementation-defined way.

The RFC has changed, since the publication of \cite{vasseur-iot}, to
drop the support for mixed networks of storing and non-storing
nodes. Now either every router can maintain its own routing table, or
none of them do and the root has to source-route each packet destined
to an RPL node.

An optional optimization for point-to-point traffic also enables the
construction of direct routes to neighbors using multicast DAOs.

\subsection{Objective Function 0}
The actual way to compute metrics, constraints and rank values
starting from the state of links and nodes is not specified in the
main RFC \cite{rfc6550} and it is left to the implementation, in
particular to the definition of the objective function.
%
To help in the design of custom OFs many useful objects, with their
related packet formats, have been defined in \cite{rfc6551} to be used
as metrics or constraints. Such objects include, for instance, the
``Link Quality Level'', ``Expected Transmissions'' (ETX),
``Hop-Count'' or ``Delay''.

In the examples that follow the chosen OF is the most simple one,
called ``Objective Function 0'' and defined in \cite{rfc6552}. This OF
does not use any object beside those defined in the RPL specification
\cite{rfc6550} and the only optimization criterion that it follows is
the minimization of the rank of the nodes, without additional metrics
or constraints.
%
In OF0 each link is assigned a dynamic value, called
\texttt{rank\_increase}, in an implementation-dependent way and the
ranks that a node can assign itself are, for each of its parents, the
sum of the rank of the parent plus the corresponding link's
\texttt{rank\_increase}.
%
Every node selects a single preferred parent, choosing the parent that
provides the minimum rank, and the next best node as a ``backup
feasible successor'' to fall back to when the preferred parent is
unavailable. OF0 does not define any delay in the choice of the
parent: as soon as a better parent is available it switches to
it. This may cause routing instability if the rank changes too
frequently. There are also other preferences defined, which are
relevant in the following sections, like giving precedence to grounded
DODAGs over floating DODAGs, in order to maintain connectivity to the
Internet, and to newer versions of the same DODAG, to allow the global
repair mechanism to succeed.

Unfortunately the RFC \cite{rfc6552} leaves the choice of the DAO
parents unspecified beyond the basic restriction that they must be a
subset of the DODAG parents, which comes from the main RPL
specification \cite{rfc6550}. It is also unclear whether the DODAG
parents are considered to be only the two above-mentioned nodes
(preferred node and backup feasible successor) or if the OF should
take into account more parents, especially in the determination of the
DAO parent set.

\section{Repair-triggering events}
When an RPL network succeeds, after its initialization, in constructing
a grounded DODAG according to the protocol rules, it is inevitable,
after some time has passed, that the established routes will become
sub-optimal or cease to exist. The nodes of the network must be able
to detect such changes in the available routes to trigger some
re-optimization procedures.

\subsection{Link and node state changes}
The most common cause for the change in route optimality will be the
variability of the state of the wireless channels between the routers:
a link between neighboring nodes may experience varying levels of
noise, interference and attenuation so its quality (for some
objective-function-specific definition of ``quality'') could improve
or worsen, up to becoming unusable.

The main RPL specification does not mandate a specific method to
verify whether a neighbor is reachable, but it does suggest a couple
of possible strategies. In contrast to traditional routing protocols,
like OSPF, that periodically send ``Hello'' packets to neighbors to
check their reachability, RPL encourages the use of a method that
avoids this kind of periodic polling, since the nodes involved are
typically much more resource-constrained than OSPF routers and this
kind of traffic would have a significant impact on the network's
resources.
%
One proposed mechanism to detect link failures is to exploit, through
the primitives defined in \cite{rfc5184}, some features of the
underlying link-layer protocol that can indicate the presence of
bidirectional connectivity to some other node. For instance, if the
RPL network is running over IEEE802.15.4, the transmitting node could
request an acknowledgement of the frames it sends and, from them,
infer the reachability of the destination.
%
Another protocol that is suggested for the detection of unreachable
nodes is the ``Neighbor Unreachability Detection'' used in IPv6
Neighbor Discovery \cite{rfc4861}. This mechanism, instead of relying
on the lower layer, exploits hints from the upper layers on the
successful transmission of packets through the current default route.
Such hints could be provided, for instance, by the replies to CoAP
confirmable messages. The RFC specifically suggests using TCP ACKs as
hints, but this protocol is rarely used in an IOT context due to its
complexity.
%
If there are no kind of hints available from the upper layers, the
Neighbor Unreachability protocol must fall back to using Neighbor
Solicitation and Neighbor Advertisement messages to probe the
neighbors.

Link state changes that do not imply a loss of connectivity but simply
a degradation or improvement of the link metrics are detected during
the periodic multicast of DIO messages, which contain the metric
objects required by the specific objective function in use.

Beside being generated from the wireless channel state's variations,
the metric changes could also be produced by changes in the state of
the RPL nodes, especially if some constraints are defined on them by
the objective function.
%
Some solar-panel-powered nodes, for instance, could be constrained to
be only leaf nodes at night, to save energy, while being full routers
capable of forwarding traffic during the day.
%
Or there could be some battery-powered nodes that must stop being
routers when their battery-level falls below a certain threshold.

Both this kind of events are experienced in a similar way by the nodes
whose links have changed state and by the nodes whose neighbors have
changed state. In both cases some nodes see a variation in the path
cost toward the root associated to some of their neighbors.

\subsection{Routing loops}
Another kind of event that can trigger a repair of the DODAG is the
detection of a loop in the routing tables.
%
The RPL protocol is designed taking into account the risk of forming
loops in the routing tables whenever a node selects a preferred
parent. The most important loop-prevention rules, in the context of
repairing mechanisms, that are defined in \cite{rfc6550} require that
\begin{itemize}
\item every node must have a rank greater than each of its parents,
\item a leaf node must advertise a rank value of
  \texttt{INFINITE\_RANK} (even if it can reach the root and the rank
  is not actually infinite),
\item a non-leaf node must have a maximum rank value of \texttt{L +
  DAGMaxRankIncrease}, where \texttt{L} is the minimum rank ever
  assigned to the node in the current DODAG version.
\end{itemize}
The importance comes from the effect, that will be seen in the
following sections, on the route building and repair mechanisms,
especially those triggered during local repair operations.

This rules, however, cannot guarantee that the DODAG will remain
loop-free. It may still be possible for a router to increase its rank
and attach to one of its former children, thus creating a loop, if the
DIO messages that advertise the change of rank get lost.
%% As an example, suppose that a node is not able to meet the
%% requirements on the rank for non-leaf nodes after a link-state change
%% and so it becomes a leaf node. It will then send out DIOs that
%% advertise an \texttt{INFINITE\_RANK}, to inform its children that they
%% must not consider it a parent any longer. But if some of those packets
%% are lost and the node, being now a leaf, attaches to one of its former
%% children there is the chance that its new parent still considers it to
%% be its own parent,thus forming a routinng loop.

To allow the detection of loops in the DODAG after their creation, RPL
introduced an IPv6 option header to be used in data packets that, like
every RPL control packet, carries the rank of the sending node and the
supposed direction of the packet (toward the root or away from the
root). Whenever the supposed direction is inconsistent with the rank
of the traversed nodes, a cycle is detected and a repair mechanism can
be triggered.

\section{Repair mechanisms}
After an RPL router has detected one of the inconsistencies or
sub-optimal configurations explained in the previous section, it
should act to repair it.
%
The specification \cite{rfc6550} does not mandate anything regarding
the repair operations in addition to the satisfaction of the basic
constraints on the rank, listed in the previous section, the inclusion
relationships between the neighbor set, the DODAG parent set and the
DAO parent set and the rules on the DODAG version change, which will be
explained in the global repair section.
%
Instead, it gives some recommendations or suggestions on how to
proceed when a re-optimization is needed or the DODAG is found to be
broken at some point, but needs most of the reaction mechanisms to be
defined by the specific objective function in use or leaves them
entirely to the implementers.

The RPL protocol subdivides repair attempts in two classes: local
repairs and global repairs, which are explained in detail in the two
following sections.

\subsection{Local repair}
This first kind of repairs are carried out or triggered by the RPL
router that detects an optimization opportunity or an inconsistency
without the need of coordination with any other node.
%
Of course the successful repair depends on the subsequent reaction of
the neighboring nodes to the repair mechanism that is being triggered.

As said in the previous sections, the most common events that can
cause a modification in the optimal routes are link state changes,
since the RPL protocol typically runs over wireless networks.
%
The link state influences the computation of the rank, metric and
possibly constraints and can lead to the loss or
establishment of connectivity between neighboring nodes.

\subsubsection{Route stability}
The choice of OF is extremely important for the performances of the
RPL network: the stability of the routes is dependent on the way the
rank and the metric are computed. Indeed the RPL RFC \cite{rfc6550}
recommends specifically to use a very smoothed value for the
computation of the rank, since an unstable rank value would also
increase the chances of forming routing loops.
%
Having unstable routes has also other negative effects: it can
increase considerably the amount of control messages exchanged in the
RPL network, hence depleting the energy reserves of battery-powered
nodes and reducing the amount of resources that remain available to
deliver useful data packets.
%
The increase in the rate of control messages is caused by the use of a
``Trickle timer'' \cite{rfc6206} to control the scheduling of DIO
messages.

The trickle timer schedules the next transmission at a random time,
uniformly picked in an interval whose lower and upper bound both
increase exponentially up to a configurable limit. The frequency of
DIO messages decreases as no changes in the topology are observed, but
when some new information is received via a DIO message from a
neighbor, or observed directly by the node, the interval of the
trickle timer is reset to its initial value, so the node can quickly
disseminate this new information and converge to a new set of routes.
%
The default values given by \cite{rfc6550}, which can be changed
administratively at the DODAG root, allow nodes to transmit DIOs as
quickly as one every \SI{8}{\ms} or as slowly as one every
\SI{2.3}{\hour} depending on the frequency of the observed changes in
the link states.

\subsubsection{Single-link state-change}
In the case where a node detects a change in the path cost, as defined
by the metric in use, of one or more of the possible paths toward the
root, it could react by picking a new set of parents among its
neighbors and a new preferred parent among the possible parents. The
exact procedure in which this happens is implementation-defined and
depends, specifically, on the objective function: in the case of OF0
the nodes always pick, without delay, the neighbor that provides the
minimum rank as preferred parent and, as backup successor, the next
best neighbor. Some other objective functions could operate
differently, for example the ``Minimum Rank with Hysteresis Objective
Function'', defined in \cite{rfc6719}, favors route stability by
ignoring changes in the metric values until they exceed a configurable
threshold.

As an example of repair that follows the degradation of a single link,
consider the four nodes represented in Fig.\ref{fig:ex_orig}, they
chose their optimal preferred parent according to OF0 and set up their
routes. Then the \texttt{rank\_increase} worsens on link AC (changing
from 1 to 4), node C detects this and recomputes its preferred parent,
which turns out to be node B. This implies a rank increase from 11 to
13 (see Fig.\ref{fig:ex_one_link}) of node C, which is advertised via
a DIO message to node D. Node D just updates its rank from 12 to 14,
but it would also choose to switch parents if a better one was
available. The reception and sending of DIOs that cause a change in
the routing information also resets the trickle timer intervals of all
involved nodes (C and D) to adapt quickly to eventual new changes and
mitigate the effect of the possible loss of DIO packets.
\begin{figure}
  \centering
  \begin{subfigure}{0.45\textwidth}
    \centering
    \import{figures/}{example_orig.tex}
    \caption{Initial situation.}
    \label{fig:ex_orig}
  \end{subfigure}%
  \begin{subfigure}{0.45\textwidth}
    \centering
    \import{figures/}{example_one_link.tex}
    \caption{After the AC link changes state.}
    \label{fig:ex_one_link}
  \end{subfigure}
  \caption{Example of a portion of RPL network. The solid lines
    represent the physical topology, the arrows denote the preferred
    parents according to OF0. The numbers between parenthesis are the
    ranks of each node, while the \texttt{rank\_increase} is shown
    over each link. The backup feasible successors are not shown and
    the dashed lines represent a multihop link to a grounded root. }
\end{figure}

\subsubsection{Detaching from the DODAG}
Another situation to consider in the repairing of the routes is what
happens when a router is not capable of forwarding packets any longer,
either because it lost every parent and cannot reach the root node or
because of a constraint that is not being met (e.g. its battery level
is below a certain threshold).
%
When a node finds itself in this situation it should, first of all,
stop other nodes from selecting it as parent by poisoning the routes
that pass through it.
%
The route poisoning operation consists in advertising an
\texttt{INFINITE\_RANK} through DIO messages to force eventual
neighbors that are relying on the leaving router to find another
parent.
%
Then the former router has the choice between waiting for a new route
to the root to become available through some neighbor and attach to it
or to start another DODAG and be its root. Typically, a node that
doesn't want to be a router for resource-related reasons will never
choose this last option, it will instead wait for a suitable neighbor
to reattach to as a leaf node.
%
If the node decides to be the root of a new floating DODAG, it will
typically be less preferred than the previous DODAG during the parent
selection. For instance, OF0 prefers parents that are part of a
grounded DODAG, and the new local DODAG is unlikely to ever become
grounded (e.g. connected to the Internet). Still, it may be useful to
maintain local connectivity during a network partition event.

An example of this situation is shown in
Fig.~\ref{fig:ex_poison}~and~\ref{fig:ex_reattach}: node C loses
connectivity to all of its parents, hence it poisons the route from D
through C by sending an \texttt{INFINITE\_RANK} DIO message to D and
then it becomes the root of a new DODAG.
%
Node D has the choice of using an alternate route through A or
following C in the new DODAG. Typically the objective function will
prefer, and it is the case for OF0, to stay in the old DODAG,
especially if it is grounded, instead of following the preferred
parent in a new floating DODAG.
%
So D will pick A as its preferred parent and advertise its new rank of
19 to C. Node C will also, if using OF0, prefer to reattach to its
previous DODAG and select D as its preferred parent.
\begin{figure}
  \centering
  \begin{subfigure}{0.45\textwidth}
    \centering
    \import{figures/}{example_poison.tex}
    \caption{Node C poisons the routes and detaches.}
    \label{fig:ex_poison}
  \end{subfigure}%
  \begin{subfigure}{0.45\textwidth}
    \centering
    \import{figures/}{example_reattach.tex}
    \caption{Node C finds a new route and reattaches to D.}
    \label{fig:ex_reattach}
  \end{subfigure}
  \caption{Example of node that loses connectivity to every parent,
    roots a new DODAG, then reattaches to the original DODAG.}
\end{figure}

\subsubsection{Repair of routing loops}
The last case is the one of routing loops, which happens in two
distinct ways:
\begin{itemize}
  \item when a node with rank L receives a packet that is going
    toward the root from a node with rank less than L,
  \item when a node with rank L receives a packet that is going away from the root from a node with rank greater than L.
\end{itemize}

In the first case the node drops the looping packet and resets the
trickle timer, which causes the sending of a multicast DIO. The DIO
will inform the sending neighbor of the current rank and allow it to
either adjust its own rank to satisfy the RPL rules or to select a
different parent.

If the routing loop, instead, is detected in the downward direction
the node that detects it can act differently depending on the mode of
operation: in non-storing mode it informs the root node with an ICMP
``Error in Source Routing'' message, which has the same format of the
standard ``Destination Unreachable'' error message, while in
storing-mode the router sends the packet back to the parent that
originally forwarded it with the ``Forwarding-Error'' flag set. This
flag will cause the parent router to drop the downward route through
this node and try again to forward the packet.

\subsection{Global repair}
As described in the previous section each router can react
independently as soon as it detects an inconsistency in the DODAG to
restore the connectivity with the root node. However such local
repairs may lead to a sub-optimal route graph, since the nodes are
restricted in their change of rank by the loop-prevention rules.
%
%% In particular if \texttt{L} is the minimum rank ever held by a certain
%% node, that node is constrained to either have a rank that doesn't
%% exceed \texttt{L + DAGMaxRankIncrease} or to become a leaf node and
%% advertise a rank of \texttt{INFINITE\_RANK}.
The parameter \texttt{DAGMaxRankIncrease}, defined in the previous
sections to be a bound on the rank increase, is administratively set
at the DODAG root node and, in some circumstances, could even be zero,
thus preventing the routers from ever increasing their rank.
%
Starting from the setting of Fig.~\ref{fig:ex_orig}, supposing that
\texttt{DAGMaxRankIncrease = 2} and that the links AC and BC have a
\texttt{rank\_increase} that worsens, respectively, from 1 to 9 and
from 1 to 2, the end result is shown in Fig.~\ref{fig:ex_rank}.
%
What happens is that, unlike in the previous situation
(Fig.~\ref{fig:ex_one_link}), node C cannot simply switch its
preferred parent over to B and increase its rank. The rank of node C
becomes 14 but this violates the above mentioned loop-prevention rule,
so C must become a leaf node and advertise an \texttt{INFINITE\_RANK}.
This, by itself, does not imply a sub-optimal routing situation, since
C is still using an optimal default route, but the same rank-increase
problem happens for D, which, as a result of the infinite rank
advertised by C, does not have the option of selecting C as its
preferred parent. Thus D is forced to become a leaf node too and to
pick a sub-optimal default route through A.
\begin{figure}
  \centering
  \import{figures/}{example_rank.tex}
  \caption{Situation after the worsening of links AC and BC: C, D must
    be leafs due to the loop-prevention rules. So D is left with a
    sub-optimal default route.}
  \label{fig:ex_rank}
\end{figure}

As it can be seen from the example, the choice of the parameter
\texttt{DAGMaxRankIncrease} provides a trade-off between the
prevention of loops in the routing tables and the freedom of the
routers to re-optimize the routes.

To resolve the problem of the non-optimality after a local repair, the
RPL specification allows the root node to trigger a global repair
operation when a global re-optimization of the DODAG is required. The
RFC does not specify the mechanisms that should trigger such global
re-optimization and leaves this choice to the implementers.
%
Some suggestions given by \cite{rfc6550} include a periodic trigger
caused by a timer, an occasional administrative intervention, a
detection of the loss of connectivity or routing inefficiency by the
upper-layer protocols (e.g. noticing an application-level delay
increase due to the longer routes).

The global repair operation simply consists in the increment of the
\texttt{DODAGVersionNumber} at the root node and the reset of its
trickle timer. This causes the creation of an entirely new routing
graph, where the nodes can safely choose a parent without being
constrained by the loop-prevention rules. When a node sees DIO
messages with the new version for the first time, its children are
still in the old DODAG version so there is no possibility of forming a
loop.
%
Care must be taken, however, during the forwarding of data packets
from the old version to the new version. Indeed the routers are
supposed to replace the rank stored in the data packet header that
they forward with \texttt{INFINITE\_RANK} while they are in the
process of switching from the old version to the new one, since they
do not know yet their own rank in the new graph. This is necessary to
avoid a false alarm in the loop-detection mechanisms that could be
triggered by an inconsistent value of the rank stored in the packet.

The precise procedure and timing to switch to the new DODAG is another
implementation-dependent detail since it involves the selection of the
parents among the neighbor nodes that belong to the new DODAG
version. The OF0, for instance, always prefers more recent versions of
the routing graph when they are available, on the condition that their
root nodes are grounded.

A switch to a new version proceeds almost exactly as a fresh
initialization of the DODAG. In the previous example, if the two links
AC and BC were the only changes that happened when the version is
incremented, then the chosen parents would the same as
Fig.~\ref{fig:ex_one_link}, i.e. the optimal ones.

The fact that the details of this mechanism are left to the
implementers can cause large differences in behavior and performance
of networks using different implementations. The complete rebuilding
of the DODAG is a very expensive operation in term of number of
packets exchanged, since it involves a complete flooding of the
network with DIO packets, and then, if the network also maintains
upward routes, with another flood of DAO messages to update them. So a
global repair should be triggered sparingly.

Another requirement from the RPL RFC is that the nodes must retain
knowledge of their previous DODAGs for some time, even if they detach
or move to another version of the DODAG. This is needed in order to
satisfy the loop-prevention rules in the case where a node switches
back to a previous DODAG after some time: if it does not know what was
its minimum rank, it could attach as a router with a too large rank.

\subsection{Faulty nodes isolation}
The RPL specification also recommends implementers to isolate nodes
that emit malformed messages at unacceptable rates.
%
This provision is of questionable utility, since the nodes typically
communicate through a wireless channel and a malfunctioning or, worse,
a malicious node could still easily disrupt communication between its
neighbors even if they ignore its messages and avoid transmitting to
it.

\section{Conclusion}
Like many other aspects of the RPL protocol, criticized in
\cite{clausen-rpl-experiences} and \cite{rpl-os-impl}, the repairing
mechanisms are severely under-specified in the main RFC and, even
taking into account the basic objective function zero, a lot of
details remain quite ambiguous. As an example, there is no mention
anywhere in \cite{rfc6550} or \cite{rfc6552} of the mechanisms to
select the DAO parents from the set of DODAG parents.
%
Even the definition of OF0 leaves open the possibility of varying many
things regarding the selection of the parents, up to the point that it
may be difficult for two different, but compliant with the RFC,
implementations to coexist.
%
Moreover the local repair mechanisms are not described in much detail
anywhere in the RPL RFC, leaving implementers to choose from various
recommendations and suggestions what they think is the best solution
for their use case. This can lead to loops and the dropping of packets
in certain corner cases described in \cite{clausen-rpl-experiences} if
the implementation is not very careful.

This freedom in the implementation details, like also written in
\cite{clausen-rpl-experiences}, on one hand makes RPL a very adaptable
protocol, suited to being used in many different situations, but on
the other hand it makes RPL too complex to be used on
resource-constrained devices, especially if they have to operate as
full routers. The many optional features make also interoperability
between different implementations very difficult.

As another example of the complexity of this protocol, none of the
four open-source implementations cited by \cite{rpl-os-impl} implement
any security feature, while each of them implements the repair
mechanisms with significant differences from the others.

\printbibliography
\end{document}
