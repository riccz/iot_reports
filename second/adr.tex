\section{Adaptive Data Rate}
\label{sec:adr}
The LoRaWAN specification \cite{lora-spec} includes a very useful
mechanism, called Adaptive Data Rate (ADR), that allows the network
server to adjust the parameters used for uplink transmissions by each
end-device.
%
When ADR is enabled, i.e. both the end-device and the network server
set the ADR flag to one in the FCtrl field of the packets they send,
the configured channels can be dynamically enabled and disabled for
each end-device (e.g. to prevent them from transmitting on a
poor-quality channel), a specific data rate is selected for uplink
transmissions, an upper limit on the uplink transmission power is set
and the number of replicas of unconfirmed messaged to be transmitted
is chosen (e.g. to provide a certain level of reliability).
%
The downlink parameters, as summarized in section~\ref{sec:lorawan}, do
also depend on the settings changed by ADR: the downlink data rate
used in the first receive window is a function of the uplink data rate
and RX1DROffset. The downlink channel used during RX1, by default,
also depends on the uplink channels in a different way according to
the region, but the mapping can be changed by issuing a DlChannelReq
command.

As already seen in figure~\ref{fig:power} each end-device should, in
order to maximize its battery life, transmit at the highest data rate
that still allows for the correct reception of the packets at some of
the gateways.
%
This is because the higher the data rate is, the lower the energy
consumption and the time needed for transmission are.
%
Another advantage is that, if the end-devices use the maximum data
possible, the network capacity is higher because the packet
transmissions use less air-time.

The standard does, indeed, recommend that each end-device controlled
by ADR use always the fastest data rate possible. So the network
server could use the information on the link quality reported to it by
each gateway (e.g. the SNR, the RSSI and, possibly, the BER) and the
variance of these parameters to determine how often to adjust the ADR
settings and which values to select in order to track the maximum data
rate that gives the desired level of reliability.

The network server (with the information provided by the gateways) is
in a better position than the end-devices to run the algorithms that
select the ADR settings: it has a much higher computational power and
it collects data about the channel between the end-device and every
gateway in its coverage range. Typically there are also more messages
that flow in the uplink direction than the reverse, especially in the
case of class A devices, so it is possible to collect information
about the channel more frequently.

When the network server wants to adjust the ADR settings of some
end-device it can send it a specific MAC command, named LinkADRReq,
and the device will reply to this message with a LinkADRAns command.
Both of them have a command identifier (CID) value of 0x03.

\subsection{LinkADRReq}
The LinkADRReq command is sent in the downlink direction carrying the
4-byte payload shown in figure~\ref{pkt:adrreq_payload}. It is used to
adjust the end-device parameters already mentioned in the previous
paragraphs, whenever the network server deems necessary, subject to
the constraint that, for class A devices, all downlink transmissions
must follow an uplink transmission.

The first field in the payload is named DataRate\_TXPower (see
figure~\ref{pkt:datarate_txpower}) and carries both the data rate and
the maximum power to be used for uplink transmissions.
%
The values that the two subfields can take and their meaning are
region-dependent and are enumerated in the standard \cite{lora-param}.
%
DataRate can be set to any value that is defined for the current
region and is compatible with the channel configuration of the
end-device (see section~\ref{sec:linkadrans}), as an example in the
EU863-870 region the defined values are those in the range from 0 to 7
and are shown in table~\ref{tab:eu_rates}.
%
The values for TXPower are, like DataRate, a different number and
correspond to different maximum power levels in each region, according
to the specific regulations.
%
As an example, in the EU863-870 region the power levels are numbered
from 0 to 5 and correspond to ERPs that decrease from \SI{20}{\dBm} to
\SI{2}{\dBm} (see table~\ref{tab:eu_power}).
%
\begin{figure*}
  \centering
  \begin{bytefield}[bitwidth=5em,bitheight=3em]{4}
    \bytefieldsetup{leftcurly=.,leftcurlyspace=0pt}
    \begin{leftwordgroup}{\tiny Byte index \vspace{1em}}
      \bitheader[endianness=little]{0-3}
    \end{leftwordgroup} \\
    \bitbox{1}{DataRate\_ \\ TXPower} &
    \bitbox{2}{ChMask} &
    \bitbox{1}{Redundancy}
  \end{bytefield}
  \caption{Structure of the payload of the LinkADRReq command.}
  \label{pkt:adrreq_payload}
\end{figure*}
%
\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em]{8}
    \bitheader{0-7} \\
    \bitbox{4}{DataRate} &
    \bitbox{4}{TXPower}
  \end{bytefield}
  \caption{Bit allocation in the DataRate\_TXPower field.}
  \label{pkt:datarate_txpower}
\end{figure}
%
\begin{table*}
  \centering
  \begin{tabular}{ccc}
    DataRate & Configuration & Indicative physical bit rate \\ \hline
    0 & LoRa: SF12 / \SI{125}{\kHz} & \SI{250}{\bps} \\
    1 & LoRa: SF11 / \SI{125}{\kHz} & \SI{440}{\bps} \\
    2 & LoRa: SF10 / \SI{125}{\kHz} & \SI{980}{\bps} \\
    3 & LoRa: SF9 / \SI{125}{\kHz} & \SI{1760}{\bps} \\
    4 & LoRa: SF8 / \SI{125}{\kHz} & \SI{3125}{\bps} \\
    5 & LoRa: SF7 / \SI{125}{\kHz} & \SI{5470}{\bps} \\
    6 & LoRa: SF7 / \SI{250}{\kHz} & \SI{11000}{\bps} \\
    7 & GFSK: \SI{50}{\kilo\bps} & \SI{50000}{\bps} \\
    8 \dots 15 & RFU &
  \end{tabular}
  \caption{Data rates defined for the EU863-870 region.}
  \label{tab:eu_rates}
\end{table*}
%
\begin{table}[h]
  \centering
  \begin{tabular}{cc}
    TXPower & ERP \\ \hline
    0 & \SI{20}{\dBm} \\
    1 & \SI{14}{\dBm} \\
    2 & \SI{11}{\dBm} \\
    3 & \SI{8}{\dBm} \\
    4 & \SI{5}{\dBm} \\
    5 & \SI{2}{\dBm} \\
    6 \dots 15 & RFU
  \end{tabular}
  \caption{Power levels defined for the EU863-870 region.}
  \label{tab:eu_power}
\end{table}
%
The end-devices are allowed to transmit with a lower power than the
configured one, so if they cannot transmit at such power they will use
the maximum transmission power their radio hardware is capable of.

The second field of LinkADRReq, shown in table~\ref{pkt:chmask}, is
named ChMask and is a 16-bit-long bit-mask that is used to dynamically
enable and disable each one of the channels that were configured on
the device. The mapping from the bits to the configured channels is
region-dependent and is controlled by the ChMaskCntl portion of the
Redundancy field (see figure~\ref{pkt:redundancy}).
%
In EU-like regions, which allow the configuration of a total number of
16 channels, the bits of the ChMask field are either directly mapped
to the configured channels or ignored, depending on the value that
ChMaskCntl takes (see table~\ref{tab:eu_chmask}). In the latter case
all the configured channels are enabled independently of the ChMask.
%
\begin{table}
  \centering
  \begin{tabular}{cc}
    Bit number & Usable channels \\ \hline
    0 & Channel 1 \\
    1 & Channel 2 \\
    \vdots & \vdots \\
    15 & Channel 16
  \end{tabular}
  \caption{Bit allocation in the ChMask field.}
  \label{pkt:chmask}
\end{table}
%
\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em]{8}
    \bitheader{0-7} \\
    \bitbox{1}{RFU} &
    \bitbox{3}{ChMaskCntl} &
    \bitbox{4}{NbTrans}
  \end{bytefield}
  \caption{Bit allocation in the Redundancy field.}
  \label{pkt:redundancy}
\end{figure}
%
\begin{table}[h]
  \centering
  \begin{tabular}{c>{\centering\arraybackslash}p{3.5cm}}
    ChMaskCntl & ChMask applies to \\ \hline
    0 & Channels 1 to 16 \\
    1 & RFU \\
    \vdots & \hspace{0cm} \vdots \\
    5 & RFU \\
    6 & All channels ON, ChMask is ignored \\
    7 & RFU
  \end{tabular}
  \caption{Allowed values for the ChMaskCntl field in the EU863-870
    region. They are the same for all the other EU-like regions and
    South Korea.}
  \label{tab:eu_chmask}
\end{table}
%
Instead in US-like regions, which can have more than 16 configured
uplink channels, the value of the ChMaskCntl field selects which block
of 16 channels the ChMask applies to. As in the EU-like regions there
are also some settings that give the possibility of enabling many
channels all at once; e.g. in the US902-928 region it is possible to
enable all the \SI{125}{\kHz} channels and apply the ChMask to the
\SI{500}{\kHz} channels with a single LinkADRReq command.

The Redundancy field contains the ChMaskCntl subfield, which was
already discussed, and the NbTrans field (see
figure~\ref{pkt:redundancy}), which configures the number of times,
between 1 and 15, that an unconfirmed uplink packet is transmitted by
the end-device.
%
A zero-valued NbTrans lets the end-device use a default value that is
not specified in \cite{lora-spec, lora-param}.
%
Each time the device transmits the same packet, it selects a new
random channel among the enabled ones, as it would in the case of a
new message, in order to increase the transmission reliability.
%
It must also open the two receive windows as usual after each one of
the repeated transmissions.
%
This means that, when the transmitting device receives a downlink
packet in the first or second (except for class C devices) receive
window, it can infer that the last uplink message was successfully
received by the network.  Therefore it can stop the repeated
transmissions before reaching the configured NbTrans value.
%
The redundant transmissions are not performed in the case of uplink
confirmed messages, since they are retransmitted anyway until an ACK
is received within the timeout interval.

\subsection{LinkADRAns}
\label{sec:linkadrans}
An end-device that receives a LinkADRReq should reply to the network
server with a LinkADRAns in order to acknowledge the correct reception
of the command and the consistency of the settings it carries with
respect to the capabilities of the device and its current state.
%
The payload of the LinkADRAns is a single byte that contains three
flags to acknowledge separately the channel mask, the power level and
the data rate settings (see figure~\ref{pkt:adrans_payload}).

The ChMask is rejected by setting the Channel mask ACK to zero in case
some of the channels that would be enabled have not been configured or
in case the bit-mask would leave the end-device without usable
channels, i.e. every configured channel has its ChMask bit set to
zero.

The Data rate ACK is set to zero if the device does not implement the
value of the DataRate that is selected and when the requested data
rate is not compatible with the settings of any of the configured and
enabled channels.
%
An example of the first case would be a US end-device that receive a
request to use DataRate 5, which is not defined for this region in the
standard \cite{lora-param}.
%
An example of the second case, instead, would be a European device that
has no channels configured beyond the three predefined ones and that
receives a request to use DataRate 6, which it supports but can not
use since the three default channels restrict the rate to DR0-DR5.

Finally, the Power ACK is set to zero if the device does not implement
the selected power level. Since the TXPower field gives an upper
limit, this happens when the radio hardware of the end-device is only
capable of transmitting using a power higher than the desired one.

\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em,bitheight=4em]{8}
    \bitheader{0-7} \\
    \bitbox{5}{RFU} &
    \bitboxes{1}{{Power \\ ACK} {Data rate \\ ACK} {Chan-\\nel mask \\ ACK}}
  \end{bytefield}
  \caption{Bit allocation in the payload of the LinkADRAns command.}
  \label{pkt:adrans_payload}
\end{figure}

The LinkADRReq is considered successful when the corresponding
LinkADRAns has all the three ACK bits set to one. In case of partial
acknowledgment (i.e. only one or two ACK bits are set to one) the
rejected settings do not affect the state of the end-device, so the
network server is always aware of the current configuration.
%
The partially acknowledged settings, however, do change the state: as
an example let a European device receive a LinkADRReq that enables
only channels 0-2 (the three \SI{125}{\kHz} default ones), sets the
data rate to 6 (SF7, \SI{250}{\kHz}) and the power level to 0
(\SI{20}{\dBm}); the device acknowledges the power level and the
channel mask, since they are consistent with its current state and
capabilities, by setting to one the corresponding bits in the
LinkADRAns payload, and will start using these settings; the data rate
is, however, not consistent with the channel configuration, so the
corresponding ACK bit will be zero and the previous rate will still be
used.

One thing that is not specified by the standard is when the end-device
can start to use the newly configured settings. It could, in fact,
update its configuration immediately and transmit also the LinkADRAns
using the new settings, or it could continue to use the old settings
until it is sure that the network server received the LinkADRAns.
%
Both options should not pose a connectivity problem, because the
end-device is still restricted to the configured channels and it is
unlikely that a channel was configured if there are no gateways
listening on it.
%
This also allows the end-device to schedule the LinkADRAns response in
order to not waste a whole packet to carry three ACK bits and,
instead, piggyback the command on a data packet.
%
So it would be reasonable if the end-device applied the new settings
immediately to improve the responsiveness to the variation of the
channel conditions, and it sent the LinkADRAns in the FOpt field of
the next data packet. It could also choose to send it as a confirmed
packet to guarantee that the network server will receive it.

\subsection{Blocks of ADR commands}
\label{sec:blocks}
The two MAC commands used by ADR are quite short: the total length,
including the CID, of a LinkADRReq is 5 bytes and the LinkADRAns are
2-byte long. The small size of the commands makes it possible to
transport more than one of them in the same packet, either in the FOpt
field or in the FRMPayload.
%
The standard \cite{lora-spec} says that the network server can send a
block of LinkADRReq commands that will be processed in order as a
single unit by the receiving end-device and acknowledged with the
same number of LinkADRAns messages.
%
The multiple answers, like the requests, will be sent together in the
same packet after the whole block of requests is processed, in order
to limit the number of transmitted packets.

A block of commands allows the network server to change the
enabled/disabled status of a set of channels that spans more than one
ChMask without sending multiple packets, e.g. in the US to enable or
disable selectively the channels 1 and 17, two LinkADRReq commands are
needed since they belong to different 16-channel blocks.
%
This example, which applies in a similar way to all US-like regions
since they allow more than 16 configured channels, is also the only
use case for this mechanism. In EU-like regions and South Korea,
indeed, there is support for 16 total channels, whose status can be
set with a single ChMask.
%
Moreover the other fields (DataRate, TXPower and NbTrans) are
overwritten by the last LinkADRReq command and, therefore, it would be
useless if they were set to different values in the commands that
compose a block.

\subsection{Enabling and disabling ADR}
Adapting the data rate may not be possible if the channel quality is
changing too quickly, as could be for example in the case of an
end-device that is moving fast with respect to how often it transmits
packets.  Such a device would see different channel conditions at each
transmission, thus making the ADR optimizations not very useful.
%
To allow a LoRa network to keep operating in conditions such as these,
both the network and the end-device can choose to disable the ADR
mechanism by setting to zero the ADR flag in the FCtrl field of the
packets that they send (see figures~\ref{pkt:fctrl_up}
and~\ref{pkt:fctrl_down}).

When ADR is not enabled, the specification \cite{lora-spec} leaves the
choice of power, data rate and channels to the application layer on
the end-devices which should still try to minimize the aggregated air
time used.
%
Even when the end-device is in control of the data rate and the
channel to be used, however, it must still obey the configuration that
was pushed to it by the network server or the default values given by
the specification.
%
This is because it is likely that there are no gateways listening on
the channels that were not configured and moreover, the end-device may
not even know what frequencies to use since the channels can be set up
freely, as is the case in EU-like regions.
%
As an example, suppose that a network in Europe lets an end-device
join it and that the CFList given to the device specifies channels 3
and 4, then the network server sends a NewChannelReq to set up an
additional high-rate channel at position 10 with only data rate
DR6. If ADR is not enabled, the device still has to restrict its
transmission choices to channels 0-4 (channels 0-2 are predefined),
with data rates between DR0 and DR5, and channel 10 with data rate
DR6.

As was seen in section~\ref{sec:blocks}, the total size of the two MAC
commands used to adjust the ADR settings allows both of them to fit
easily in the FOpt field of the FHDR.
%
They also do not contain sensitive information, since the settings
they transport could be inferred by listening to all the channels for
some time. So it is reasonable to piggyback them on data packets
instead of requiring the transmission of control packets just for this
purpose.
%
This means that the network server could adjust the ADR settings every
time it sends a packet to an end-device which could be, in principle,
after every uplink transmission.
%
The increase in the number of packets received by the end-device may,
however, pose a problem of battery life because it will require more
energy to keep the radio on for the duration of the reception.
%
This implies that the decision on when to disable ADR due to the
channel variability should also take into account that if the
frequency of the adjustments needed to track the optimal data rate is
too high, it may increase the energy consumption of the end-device to
the point that it overtakes the energy savings allowed by the use of
the highest rate possible.

\subsection{Rate validation}
The ADR mechanism tries to track the highest possible data rate that
still allows an end-device to communicate with the network, but it is
possible that sometimes the channel will worsen too quickly for the
network server to adjust the data rate in time, and in this case the
end-device will lose its connectivity with the gateways.
%
If this happens the network server is unable to control the data rate,
therefore each device must be able to detect such situations and try to
restore connectivity with at least one gateway.

Indeed the LoRa end-devices periodically trigger a rate validation
mechanism that forces the network server to confirm that it is able to
receive uplink packets.
%
Each end-device keeps track of the number of uplink packets that it sent
since the last downlink packet that was successfully received,
excluding repeated transmissions, using a counter called
ADR\_ACK\_CNT.
%
When the counter reaches the value of ADR\_ACK\_LIMIT without any
downlink message, the end-device triggers a validation of the data
rate settings it is currently using.
%
The connection is tested by sending the next uplink packet with the
ADRACKReq flag of the FCtrl header field (see
figure~\ref{pkt:fctrl_up}) set to one.
%
If the network server receives such packet it will schedule the
transmission of a reply message that must be received by the
end-device with a maximum delay of ADR\_ACK\_DELAY. The server should
try to send some data in this packet, for efficiency, but it can send
even an empty packet to meet the ADR\_ACK\_DELAY deadline.
%
On reception of any downstream packet (not necessarily with the ACK
flag set), the end-device will infer that its previous packet was
correctly received by the network server and it will reset its
ADR\_ACK\_CNT.
%
If, on the other hand, the ADR\_ACK\_DELAY passes without any downlink
message then the end-device infers that the data rate in use is too
high for the current conditions of the channel.
%
It can try, in this case, to restore the connection with some of the
gateways by switching to a slower data rate, which improves the
coverage range.
%
After the rate switch, the end-device will start counting again the
uplink packets and trigger the same rate validation process if it
still doesn't receive packets from the network server.

If the device steps back to the slowest data rate allowed by the
channel configuration, the ADRACKReq flag will not be set anymore
since in this case there is nothing to be done to improve the coverage
range.
%
When the device loses connectivity it could still try to transmit as
normal, each time on a new random channel picked among the available
ones, until the conditions improve and it can reach again some of the
gateways.

The recommended values for the counter limit and the reply timeout are
region-specific, although they are currently the same for all regions
in \cite{lora-param}: ADR\_ACK\_LIMIT = 64 and ADR\_ACK\_DELAY = 32.

The fact that the device decreases the data rate of just one step each
ADR\_ACK\_LIMIT + ADR\_ACK\_DELAY transmissions may be a problem if
there are big variations in the channel attenuation. In this case,
indeed, it would be more efficient to switch directly to the lowest
rate.
%
An example of this effect could be a parking sensor buried under the
road surface: when a car is on top of it, the sensor would experience
a great increase in the channel attenuation because of the metal
placed over it and would require a lot of time to reach a data rate
slow enough one step at a time.
%
This sort of optimization, however, can only be done if more
information is available regarding the environment where the devices
are placed.

\subsection{Differences in higher classes}
Devices that belong to classes B and C must still be able to operate
as class A devices, so they too support ADR with few differences
related to their specific features.

Class B devices operate in the uplink direction in the same way as
devices of class A, that is they transmit after a random short delay
and then open two receive windows. Therefore ADR applies to them
unchanged.
%
Indeed the new channels configured for the transmission of beacons and
the reception of network-server-initiated downlink transmissions are
not subject to ADR, like all the other downlink channels.
%
Also the introduction of multicast packets makes no difference with
respect to ADR since MAC commands can be transported only by unicast
packets.
%%  Their usefulness in multicast packets would be quite limited
%% anyway, since end-devices see uncorrelated channels unless they are
%% very close to each other, hence their data rates need to be
%% optimized individually.
The possibility to send packets periodically frees the network server
of the need to schedule ADR commands after an uplink
transmission.
%
This may not however be much useful, since the information about the
state of the channel is collected during the reception of uplink
packets, therefore the two following receive windows are a good time
to send eventual setting adjustments that the new channel conditions
require.

Class C, instead, introduces a small difference in the operation of
ADR caused by the fact that the second receive window (RX2) is always
open when the class C end-device isn't transmitting or listening on
RX1.
%
This means that the reception of a packet from the network server
during RX2 is not an indication that the previous uplink packet was
received, unlike what happens for the other two classes.
%
The end-device, in this case, can only reset the ADR\_ACK\_CNT when it
receives a packet during RX1.  It also cannot stop the redundant
transmission of unconfirmed packets even if it receives a message
during RX2.
