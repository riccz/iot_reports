\section{LoRaWAN}
\label{sec:lorawan}
Like it was written in \cite{lora-dawn}, the MAC layer of LoRa was
designed to resemble the IEEE802.15.4 standard in order to ease the
integration with some widespread upper layer protocols, like 6LoWPAN
and CoAP.

The LoRaWAN MAC protocol runs only on the end-devices and the network
server (see figure~\ref{fig:stack}), not on the gateways which forward
the PHYPayloads they receive to the network server together with some
information on the channel quality.
%
This has a number of advantages \cite{lora-rising}: it simplifies the
system architecture by pushing all the complexity to the central
network server and in particular it removes the need to handle the
mobility of end-devices since they do not need to associate to a
particular gateway, also multiple gateways can forward the same packet
to the network server, so the end-devices do not need to have a good
link to one particular gateway, but just to any one of them.
%
In the downlink direction, instead, a single gateway is selected by
the network server to forward the packets to the end-device, typically
the one that sees a better channel.

There are three classes of end-devices, named A (all end-devices), B
(beacon) and C (continuously listening) with support for different
modes of packet transmission and reception. The devices in the first
class are the most simple and only implement the minimum functionality
required by all the LoRa devices.
%
Class A devices can transmit whenever they have a packet to send to
the network server, after a short random delay, on a channel chosen
randomly among the enabled ones. Then they listen in two receive
windows after two delays (RECEIVE\_DELAY1 and RECEIVE\_DELAY2) for
downlink transmissions from the network server.
%
If the uplink message required an acknowledgement and it was not
received in the two receive windows, or if the configured number of
transmissions for unacknowledged messages is greater than 1, the
end-device repeats the transmission after a random back-off time.
%
This mode of operation implies that the class A end-devices must be
able to tolerate high latency in the downlink channel, since the
network server has no way to send a packet outside the two receive
windows.

Class B end-devices, in addition to the mechanism explained above,
open a receive window periodically to listen for downlink messages. In
order to keep the clocks of the end-devices synchronized, the gateways
broadcast a beacon packet periodically.

Class C devices, finally, have no strict energy constraints so they are
listening on RX2 as long as possible: they stop listening only when
they transmit and during RX1, where they listen on a different channel.

Devices that belong to classes B and C also support multicast in the
downlink direction of packets that do not carry MAC commands.

There are 7 types of packets defined for LoRaWAN: four used for
regular network traffic (MAC commands and application data) in both
directions and with or without ACKs, two specific packets used during
the OTA activation process and one reserved for proprietary extensions
to the protocol.
%
%% Every multi-byte field defined in the following MAC packets is sent
%% over the air in little endian order. The bit order within each byte is
%% also little-endian.
%
They all have the same structure (see figures~\ref{pkt:mac}
and~\ref{pkt:mhdr}, taken from \cite{lora-spec,lora-param} like all
the figures and tables that follow): a header that contains just the
packet type and the protocol major version, the payload and a Message
Integrity Code at the end. The MIC is computed over the whole packet
using \cite{lora-dawn} the same algorithm employed by IEEE802.15.4.
The payload structure differs for join request, join response and data
packets.
%
\begin{figure*}
  \centering
  \begin{bytefield}[bitwidth=5em]{4}
    \bytefieldsetup{leftcurly=.,leftcurlyspace=0pt}
    \begin{leftwordgroup}{\tiny Byte index \vspace{1em}}
      \bitheader[endianness=little]{0-3}
    \end{leftwordgroup} \\
    \begin{rightwordgroup}{1 \dots M bytes \\ of payload}
      \bitbox{1}{MHDR} & \bitbox[tlr]{3}{} \\
      \wordbox[lr]{2}{MACPayload or Join-Request or Join-Response} \\
      \skippedwords \\
      \wordbox[blr]{1}{}
    \end{rightwordgroup} \\
    \wordbox{1}{MIC}
  \end{bytefield}
  \caption{Structure of the PHYPayload.}
  \label{pkt:mac}
\end{figure*}
%
\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em]{8}
    \bitheader{0-7} \\
    \bitbox{3}{MType} & \bitbox{3}{RFU} & \bitbox{2}{Major}
  \end{bytefield}
  \caption{Bit allocation in the MHDR field.}
  \label{pkt:mhdr}
\end{figure}

The data packets contain another header inside MACPayload (see
figure~\ref{pkt:frame}), named FHDR, which contains the address
assigned to the device during the joining process (DevAddr), a field
of control flags (FCtrl), a sequence counter (FCnt) and an optional
field (FOpt) used to piggyback short MAC commands on application-data
packets.
%
\begin{figure*}
  \centering
  \begin{bytefield}[bitwidth=5em]{4}
    \bytefieldsetup{leftcurly=.,leftcurlyspace=0pt}
    \begin{leftwordgroup}{\tiny Byte index \vspace{1em}}
      \bitheader[endianness=little]{0-3}
    \end{leftwordgroup} \\
    \begin{leftwordgroup}{FHDR}
      \wordbox{1}{DevAddr} \\
      \begin{rightwordgroup}{0 \dots 15 bytes \\ of FOpts}
        \bitbox{1}{FCtrl} & \bitbox{2}{FCnt} &
        \bitbox[ltr]{1}{FOpts} \\
        \skippedwords \\
        \wordbox[blr]{1}{}
      \end{rightwordgroup}
    \end{leftwordgroup} \\
    \begin{rightwordgroup}{0 \dots N bytes \\ of FRMPayload}
    \bitbox{1}{FPort} &
    \bitbox[tlr]{3}{FRMPayload} \\
    \skippedwords \\
    \wordbox[blr]{1}{}
    \end{rightwordgroup}
  \end{bytefield}
  \caption{Structure of the MACPayload. If FRMPayload is absent the
    FPort field is omitted too.}
  \label{pkt:frame}
\end{figure*}

The control flags are different depending on the packet direction (see
figures~\ref{pkt:fctrl_up} and~\ref{pkt:fctrl_down}). The network can
set a flag (FPending) to indicate to an end-device whether there are
more packets destined to it and, therefore, if it should open
additional receive windows to get them.  The end-devices, instead, can
set a flag (ADRACKReq) that will be explained, together with the ADR
flag, in section~\ref{sec:adr} since it is used by the adaptive data
rate mechanism.
%
The other two flags are an ACK, set when the previous message required
a confirmation, and the length of the optional Fopt field.
%
The FPort field has value 0 when the packet contains MAC commands and
will not be passed up to the application layer, or an
application-dependent value in the range $[1,223]$ when it contains
application data.

All the above packets are defined for nodes of class A. There are also
additional packets defined for the other two classes to transmit and
receive beacon messages and additional fields, like a flag in FCtrl
that advertises the class B capabilities of the end-device.

The standard also defines a set of control commands, called MAC
commands, that can be exchanged between the end-devices and the
network server. They can be carried by the optional FOpt field of the
MACPayload if they are short enough and do not require encryption, or
they can be carried by a control packet with a payload made of just a
sequence of MAC commands.
%
The structure of MAC commands is very simple: a one byte identifier
(CID) possibly followed by a command-specific list of arguments.
%
There are commands defined for communicating information about the
status of the end-nodes to the network (e.g. the battery level), to
query the quality of the link (e.g. the SNR margin at the gateways and
the number of gateways that can receive the packets) and to setup the
parameters of the physical and MAC layer (e.g. the delay to open the
receive windows, the channel data rates and frequencies).
%
There is also a CID reserved for proprietary extensions and two
commands related to ADR: LinkADRReq and LinkADRAns, which are
explained in detail in section~\ref{sec:adr}.
%
\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em,bitheight=3em]{8}
    \bitheader{0-7} \\
    \bitboxes{1}{{ADR} {ADR-\\ACK-\\Req} {ACK} {RFU}} &
    \bitbox{4}{FOptsLen}
  \end{bytefield}
  \caption{Flags inside the FCtrl field in the uplink direction.}
  \label{pkt:fctrl_up}
\end{figure}
%
\begin{figure}
  \centering
  \begin{bytefield}[bitwidth=2.5em,bitheight=3em]{8}
    \bitheader{0-7} \\
    \bitboxes{1}{{ADR} {RFU} {ACK} {FPen-\\ding}} &
    \bitbox{4}{FOptsLen}
  \end{bytefield}
  \caption{Flags inside the FCtrl field in the downlink direction.}
  \label{pkt:fctrl_down}
\end{figure}

To join the network the LoRa end-devices either have everything set up
at production time (activation by personalization) or they can perform
an over-the-air activation.
%
The first kind of activation is simple, but has the drawback that the
device will only work on one network, also the session keys are stored
on the device and, therefore, they are always the same.
%
OTAA, instead, allow a device to negotiate temporary session keys for
encryption and integrity protection of both MAC commands and
application data, and a short network-specific address to be used in
the MAC header.
%
The information that must be stored in the end-devices before the OTAA
is: two globally unique device and application identifiers (DevEUI and
AppEUI) and a per-device root AES128 key (AppKey) from which the
temporary keys are derived.

Using the session keys, either agreed upon with the network server
during the join procedure or permanently stored in the device, each
packet that is exchanged between the network server and the
end-devices is signed with the MIC and has its payload encrypted.
%
The encryption is performed by generating a keystream for each packet
from one of the two session keys, the sequence number counters in both
directions, the temporary device address and the packet direction
(uplink or downlink) and XOR-ing it with the packet payload.
%
The key to be used depends on the destination of the packet: if it
contains MAC commands it must be decrypted by the network server so it
is encrypted with the NwkSKey, if it contains application data instead
it is encrypted with the AppSKey.
%
%% The use of two keys could allow the application data to remain secret
%% to the network server, even if the generation of the session keys
%% would then require access to the root AppKey so there should be an
%% interface between the application server and the network server to
%% perform the cryptographic operations with this keys without discolsing
%% it.
%
The MIC instead is always computed using the NwkSKey so the
application payload is not integrity-protected against a compromise of
the NwkSKey.
%% protection is required also against the network operator, another
%% signature must be included in the application palyload.
