\section{LoRa physical layer}
\label{sec:phy}
As also described in \cite{lora-rising,lora-dawn}, the physical layer
of LoRa is implemented in the end-devices and the gateways, which
communicate wirelessly, while the connectivity between gateways and
the network server is ensured via any kind of link (wired or wireless)
able to provide IP connectivity(see figure~\ref{fig:stack}).
%
\begin{figure*}[hbt]
  \centering
  \includegraphics[width=0.8\textwidth]{lora_stack}
  \caption{LoRa protocol stack. From \cite{lora-dawn}}
  \label{fig:stack}
\end{figure*}

\subsection{Modulation}
The part of the specification \cite{lora-param} from the LoRa Alliance
that deals with the region-dependent parameters (e.g. the allocated
frequency bands and the restrictions on their use) mandates the use of
either the LoRa modulation, designed by Semtech \cite{lora-radio}, or
GFSK for the link between the end-device and the gateway.

The GFSK modulation is implemented in many chipsets from different
vendors and is also used in other standards, like Bluetooth
\cite{bluetooth-intro}, while LoRa is a modulation based on Chirp
Spread Spectrum (CSS) that was, until recently, implemented only by
Semtech \cite{lora-alliance}.

The improvement, as written in \cite{lora-dawn}, over CSS is given by
the continuity between the chirp symbols in the preamble of the
physical layer packet. This allows the receiver to synchronize the
timing and frequency in a simpler and more accurate way without
requiring a stable clock, which allows the end-device nodes to be
cheaper.

LoRa allows to select different lengths $2^{SF}$ for the spreading
sequence used to code the transmitted symbols, where SF is the
spreading factor that can range from 7 to 12.
%
This gives the possibility to reduce the data rate for long-distance
transmissions and increase it when the receiver is closer or the
channel conditions are good enough.
%
The spreading sequences used in LoRa were also designed to have little
interference between nodes using the same frequency band with
different spreading factors, thus allowing them to transmit at the
same time without collisions.

Simulation results displayed in figure~\ref{fig:power} show how the
spreading factor must increase (and therefore the bit-rate must
decrease) with the distance to be able to successfully transmit and
how this implies a rise in the energy and time required to complete
the transmission.
%
\begin{figure*}[tbh]
  \centering
  \includegraphics[width=0.75\textwidth]{power_dist}
  \caption{Simulation of the required energy and time as a function of
    the distance from the gateway. From \cite{lora-old-slides}}
  \label{fig:power}
\end{figure*}

\subsection{Frequency allocations}
The specific frequencies, bandwidths, spreading factors and other
parameters of the physical layer are defined in \cite{lora-param},
according to the regulations in different regions, following two base
patterns in the channel allocation that could be called ``EU-like''
and ``US-like'', since they are the first two different regions that
are listed in the specification.

The EU-like schema is implemented, with some minor variations, in
devices operating in Europe (EU863-870 and EU433), China (CN779-787)
and in the countries where the ISM band includes the interval
$[923,923.5]~\si{\MHz}$ (AS923).
%
In the available frequency bands from each region there is a small
number (between 2 and 6) of predefined \SI{125}{\kHz} channels, which
are enabled by default on end-devices and used to broadcast the initial
JoinReq packets. They must also always be enabled on the
gateways.
%
Other channels can be set up by the LoRa network with arbitrary
center-frequencies, given that they belong to the allocated band and
that the devices' hardware is able to use them, up to a total of 16
channels. The channel selection can be done either during the
over-the-air activation process, by specifying in the CFList field up
to 5 additional channels with default data rates, or at any time after
that by using a NewChannelReq command, which also allows to select the
desired data rates.

There are 7 configurations for the modulation to be used, numbered
from 0 to 7 in order of increasing bit-rate: DR0 - DR5 use LoRa with
\SI{125}{\kHz} channels and all the possible SFs in decreasing order,
DR6 uses LoRa with SF 7 on \SI{250}{\kHz} channels, DR7 uses GFSK at
\SI{50}{\kilo b\per\s}.
%
The default rates, that can be used on the predefined channels and on
the channels set up in the CFList, are DR0 - DR5.
%
The transmission power can be set to one of 5 different levels with
values that depend on the specific region.

All regions define some duty-cycle requirements for the transmission,
even those, like EU863-870, that could avoid this by listening before
transmitting use duty-cycling for simplicity. Some of the countries in
AS923 also have a dwell-time restriction that must be enabled on the
end-devices by the network when needed.

On the first receive window the end-devices listen, by default, on the
same channel that was used for the previous uplink transmission, but
they can be customized by issuing DlChannelReq commands. The data rate
that is used for the downstream packet is a function of the uplink
data rate that was used and a configurable parameter: RX1DRoffset.
%
The second window, instead, is set to a single configurable channel
and data-rate.

The US-like pattern for the physical layer parameters is implemented,
with minor variations, in North America (US902-928), Australia
(AU915-928) and China (CN470-510).
%
Here the allocated spectrum is divided in many (up to 144) predefined
channels that can be overlapping
%(US902-928 and AU915-928)
or not
%(CN470-510)
and are separated in uplink and downlink channels.
%
This implies that there is no channel-setup mechanism (CFList and
NewChannelReq) as in the EU-like regions and, also, the devices must
be able to store the configuration for each of the defined uplink
channels.

For this regions the JoinReq messages are broadcast on randomly chosen
channels, alternating between different bandwidths.
%\SI{125}{\kHz} and \SI{500}{\kHz} in
%regions that define them.

The only modulation allowed is LoRa with various values of SF and
bandwidth. In the US902-928 and AU915-928 regions there are 4 rates
(DR0 - DR3) that use \SI{125}{\kHz} channels and 7 rates with
\SI{500}{\kHz} channels (DR4 and DR8-DR13), while in the CN470-510
region the allowed rates are only 5 (DR0-DR5), all defined on
\SI{125}{\kHz} channels.
%
The power levels that are defined are 10 and the specific values
depends on the region in US902-928 and AU915-928, while there are only
7 power levels in CN470-510.
%
As in the EU-like regions there are duty-cycle and dwell-time
restrictions that are different within each region.

The channel used for the first receive window is a function of the
previous uplink channel used (uplink channel number modulo the number
of downlink channels) and the rate is selected, as in the EU-like
regions, based on the uplink rate and the RX1DROffset parameter. The
second receive window uses a single configurable rate and channel.

Finally there are the South Korean regional parameters (KR920-923),
which are a mix of both EU-like and US-like patterns: there are a
fixed number of channels already defined, but they can be enabled and
configured using the same mechanisms employed in EU-like regions:
CFList and NewChannelReq.  The only modulation allowed is LoRa on
\SI{125}{\kHz} channels (DR0-DR5 with the same SFs of the EU-like
regions) and there are 6 power levels between \SI{20}{\dBm} and
\SI{0}{\dBm}.
