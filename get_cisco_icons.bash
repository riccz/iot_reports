#!/bin/bash

set -e -o pipefail
set -x

mkdir -p cisco-icons/color cisco-icons/bw
pushd cisco-icons/bw
if [ ! -e cisco_bw_icons.zip ]; then
    wget -O cisco_bw_icons.zip "https://www.cisco.com/c/dam/en_us/about/ac50/ac47/doc.zip"
    unzip cisco_bw_icons.zip
    rename 's/ /_/g' *
fi
popd
pushd cisco-icons/color
if [ ! -e cisco_color_icons.zip ]; then
    wget -O cisco_color_icons.zip "https://www.cisco.com/c/dam/en_us/about/ac50/ac47/3015_eps.zip"
    unzip cisco_color_icons.zip
    rename 's/ /_/g' *
fi
popd
